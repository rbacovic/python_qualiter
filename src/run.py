"""
This module provides a command-line interface for running the main linter procedure.

It uses the Click library to create a CLI tool that accepts two options:
1. --path: The path to the repository (required)
2. --dry-run: A flag to perform a dry run without making changes (optional)

The main functionality is implemented in the 'main' function from the 'linter' module.

Usage:
    python run.py --path /path/to/repository [--dry-run]

This script is designed to be the entry point for running the linter on a specified repository,
providing an easy-to-use interface for developers to check and maintain code quality.
"""

import click

from linter import main


@click.command()
@click.option(
    "--path", type=click.Path(exists=True), required=True, help="Path to the repository"
)
@click.option(
    "--dry-run",
    is_flag=True,
    default=False,
    help="Perform a dry run without making changes",
)
def cli(path, dry_run):
    """Run the main linter procedure."""
    main(path, dry_run)


if __name__ == "__main__":
    cli()
