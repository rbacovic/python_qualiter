"""
This module provides functionality for running linters on Python code in a Git repository.

It includes the following main components:
- LinterConfig: A dataclass for storing linter configuration.
- GitHandler: A class for interacting with Git repositories.
- LinterRunner: A class for running multiple linters on changed files.

The module supports running various linters such as isort, black, mypy, flake8, pylint, and vulture.
It can be used to check code quality and enforce coding standards in a Python project.

Usage:
    from linter import main
    main(path="/path/to/repo", dry_run=True)

The main function initializes the linter configuration and runs the linters on the specified repository.
"""

import subprocess
from dataclasses import dataclass
from pathlib import Path
from typing import Generator, List

from git import Repo


@dataclass
class LinterConfig:
    path: str
    dry_run: bool = False

    def __post_init__(self):
        self.rules = {
            "isort": "",
            "black": "",
            "mypy": "--ignore-missing-imports",
            "flake8": "--ignore=E203,E501,W503,W605",
            "pylint": "--ignore=dags --disable=line-too-long,E0401,E0611,W1203,W1202,W0212,C0103,R0801,R0902,W0212,W0104,W0106,W0703,W0125",
            "vulture": "--min-confidence 100",
        }
        self.repo_path = Path(self.path)


class GitHandler:
    def __init__(self, repo_path: Path):
        self.repo = Repo(repo_path)

    def get_changed_files(self, file_extension: str = ".py") -> List[str]:
        """Get list of changed files with specific extension between local and remote master."""
        self.repo.remotes.origin.fetch()
        diff = self.repo.head.commit.diff("origin/master")
        return [item.a_path for item in diff if item.a_path.endswith(file_extension)]

    @property
    def current_branch(self) -> str:
        """Get current branch name."""
        return self.repo.active_branch.name


class LinterRunner:
    def __init__(self, config: LinterConfig):
        self.config = config
        self.git_handler = GitHandler(config.repo_path)

    def generate_commands(self, files: List[str]) -> Generator[str, None, None]:
        """Generate linter commands for each file and rule."""
        for file in files:
            for rule, options in self.config.rules.items():
                file_path = self.config.repo_path / file
                yield f"{rule} {file_path} {options}"

    @staticmethod
    def execute_command(command: str, check_errors: bool = False) -> List[str]:
        """Execute shell command and return output as list of strings."""
        try:
            result = subprocess.run(
                command, shell=True, check=check_errors, capture_output=True, text=True
            )
            return result.stdout.splitlines()
        except subprocess.CalledProcessError as e:
            print(f"Error executing command: {command}")
            print(f"Error message: {str(e)}")
            return []

    def run(self):
        """Run all linters on changed files."""
        print(f"Current branch: {self.git_handler.current_branch}")

        changed_files = self.git_handler.get_changed_files()
        results = []

        for command in self.generate_commands(changed_files):
            if self.config.dry_run:
                print(f"[DRY RUN]: {command}")
            else:
                results.extend(self.execute_command(command))

        for result in results:
            print(result)


def main(path: str, dry_run: bool = True):
    config = LinterConfig(path=path, dry_run=dry_run)
    linter = LinterRunner(config)
    if dry_run:
        print("Dry run mode. No changes will be made.")
    linter.run()

