from python_qualiter.src.run import cli

import pytest
from click.testing import CliRunner
from unittest.mock import patch

@pytest.fixture
def runner():
    return CliRunner()

@patch('run.main')
def test_cli_with_path(mock_main, runner):
    result = runner.invoke(cli, ['--path', '/test/path'])
    assert result.exit_code == 0
    mock_main.assert_called_once_with('/test/path', False)

@patch('run.main')
def test_cli_with_path_and_dry_run(mock_main, runner):
    result = runner.invoke(cli, ['--path', '/test/path', '--dry-run'])
    assert result.exit_code == 0
    mock_main.assert_called_once_with('/test/path', True)

def test_cli_without_path(runner):
    result = runner.invoke(cli)
    assert result.exit_code != 0
    assert "Missing option '--path'" in result.output

def test_cli_with_nonexistent_path(runner):
    result = runner.invoke(cli, ['--path', '/nonexistent/path'])
    assert result.exit_code != 0
    assert "Path '/nonexistent/path' does not exist" in result.output