import pytest
from unittest.mock import patch
from python_qualiter.src.linter import LinterConfig, LinterRunner, GitHandler, main

@pytest.fixture
def mock_config():
    return LinterConfig(path="/test/repo", dry_run=True)

@pytest.fixture
def mock_git_handler(mocker):
    mock = mocker.Mock(spec=GitHandler)
    mock.get_changed_files.return_value = ["test_file.py"]
    mock.current_branch = "test-branch"
    return mock

@pytest.fixture
def linter_runner(mock_config, mock_git_handler):
    with patch("python_qualiter.src.linter.GitHandler", return_value=mock_git_handler):
        return LinterRunner(mock_config)

def test_linter_runner_init(linter_runner, mock_config):
    assert linter_runner.config == mock_config

def test_generate_commands(linter_runner):
    files = ["test_file.py"]
    commands = list(linter_runner.generate_commands(files))
    assert len(commands) == len(linter_runner.config.rules)
    assert all(rule in cmd for cmd in commands for rule in linter_runner.config.rules)

@patch("python_qualiter.src.linter.subprocess.run")
def test_execute_command(mock_run, linter_runner):
    mock_run.return_value.stdout = "Test output"
    result = linter_runner.execute_command("test command")
    assert result == ["Test output"]
    mock_run.assert_called_once()

def test_run_dry_run(linter_runner, capsys):
    linter_runner.run()
    captured = capsys.readouterr()
    assert "Current branch: test-branch" in captured.out
    assert "[DRY RUN]:" in captured.out

@patch("python_qualiter.src.linter.LinterRunner")
def test_main(mock_linter_runner):
    main("/test/repo", dry_run=True)
    mock_linter_runner.assert_called_once()
    mock_linter_runner.return_value.run.assert_called_once()